Purpose
=======

Designed to mount an XCR3D BP6 hot end with a Volcano or
Volcano-length (Pro-length) CHC heat block, a v1.5 Orbiter extruder,
and an 8mm inductive probe to the stock carriage on an SK-Tank.

Printing
========

Do not print in PLA! PETG is a good choice because of limited
shrinkage. ABS works well and is less stringy. These are high
precision parts, at least for 3D printing!

The duct is designed to be printed with only bed supports "on
its back" with the nozzles at the top of the print.  The first
layer can be .3mm but the other layers should be fine and evenly
divide into 1mm (for example .2mm, but not 0.15).  Print slow.
The duct may require some cleaning, particularly near the fan
entrance.

The main bracket should be printed as modeled and layer thickness
is not critical. Supports will be needed for the "throat" that
holds the heat sink. That interface is tight and will need to be
cleaned thoroughly. The heat sink should be a tight fit and may
need mechanical help to seat.

The mounting strap should be printed flat in the obvious orientation,
and layer thickness is not critical. No supports are needed.

Installation
============

Install 3.5x4mm heat set inserts into each of the M3 holes except
for the three fan attachment holes. Tap the three fan attachment
holes for M3 most of the way through, leaving the back of the hole
untapped as a lock washer.

Install the fan into the holder with 3x M3x25 screws before installing
the holder onto the carriage with two M3x8 screws.

Use M3x6 screws to attach the ADXL345 board when it is in use.

Use M3x8 screws for all other locations.

The top nut on the inductive sensor intentionally rests against the
side of the bracket to lock the nut.  To adjust it, Determine how
far you want to move it in mm. Because the thread pitch on the body
of the inductive sensor is 1.25mm, each 1/6 turn of the nut (moving
the next face to the bracket) moves the sensor up or down 1.25mm/6 =
.2083mm. Measure, count turns of the nut on the body, and re-install.

Align the BP6 with the set screws for the heat break facing forward
to make access easy.  (I recommend replacing them with 10mm or so normal
M3 screws.) Then mount the fan to the right side of the BP6.  Press
the BP6 into the main bracket securely.  Fasten the mounting strap
securely with M4 screws.

Orient the Volcano heat block towards the rear or front, insert
the BP6 throat fully into the BP6 heat break, and tighten the heat
break screws.  It is close enough to the nozzle that it must wear
a sock to avoid damage to the nozzle.  Insert PTFE tube fully into
the BP6, and cut off enough to just barely fit the orbiter in place.

This design also works with Ceramic Heater Core (CHC) blocks in
place of the Volcano block. It is intended to work well with either.

Install the Orbiter into the bracket with M3x8 screws.


Tools
=====

Created with [FreeCAD](https://www.freecad.org/)


Attributions
============

* [Orbiter Models](https://www.thingiverse.com/thing:4725897)
* [SK Tank Models](https://github.com/SecKit/SK-Tank)
* [2040 Fan Model](https://grabcad.com/library/4020-blower-fan-1)
* [XCR3D BP6 Models](https://grabcad.com/library/xcr-bp6-hotend-1)

License
=======

Referenced objects used for dimensions only and are subject to their
own licenses. These models are Copyright Michael K Johnson, and
licensed for reuse under [CC-BY](https://creativecommons.org/licenses/by/4.0/)
